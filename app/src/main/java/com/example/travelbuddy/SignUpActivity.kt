package com.example.travelbuddy

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import com.example.travelbuddy.databinding.ActivitySignUpBinding
import com.google.firebase.auth.FirebaseAuth

class SignUpActivity : AppCompatActivity() {

    //ViewBinding
    private lateinit var binding: ActivitySignUpBinding

    //ActionBar
    private lateinit var actionBar: ActionBar

    //Progress Dialog
    private lateinit var progressDialog: ProgressDialog

    //Firebase Auth
    private lateinit var firebaseAuth: FirebaseAuth

    //data
    private var email = ""
    private var password = ""
    private var reenteredPassword = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //actionbar + back button
        actionBar = supportActionBar!!
        actionBar.title = "Sign Up"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayShowHomeEnabled(true)

        //configure progress dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Please wait")
        progressDialog.setMessage("Signing in...")
        progressDialog.setCanceledOnTouchOutside(false)

        //firebase auth
        firebaseAuth = FirebaseAuth.getInstance()

        //begin sign up
        binding.signUpBtn.setOnClickListener {
            //validate data and confirm re-enter data
            validateData()
        }
    }

    private fun validateData() {
        //get data
        email = binding.emailEt.text.toString().trim()
        password = binding.passwordEt.text.toString().trim()
        reenteredPassword = binding.reEnterPasswordEt.text.toString().trim()

        if (TextUtils.isEmpty(email)) {
            //email is not entered
            binding.emailEt.error = "Please enter email address"
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            //invalid email address
            binding.emailEt.error = "Please enter a valid email address"
        } else if (TextUtils.isEmpty(password)) {
            //password is not entered
            binding.passwordEt.error = "Please enter the password"
        } else if (TextUtils.isEmpty(reenteredPassword)) {
            //reEnterPassword field is empty
            binding.reEnterPasswordEt.error = "Please enter the password"
        } else if (password != reenteredPassword) {
            //password does not match
            binding.reEnterPasswordEt.error = "Password does not match"
        } else if (password.length < 6) {
            //password length is lees than 6
            binding.passwordEt.error = "Password must be at least 6 characters long"
        } else {
            firebaseSignUp()
        }
    }

    private fun firebaseSignUp() {
        progressDialog.show()

        //create account
        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                //sign up success
                progressDialog.dismiss()

                //get current user
                val firebaseUser = firebaseAuth.currentUser
                val email = firebaseUser!!.email
                Toast.makeText(this, "Account created with email $email", Toast.LENGTH_SHORT).show()

                //open profile activity
                startActivity(Intent(this, ProfileActivity::class.java))
                finish()
            }
            .addOnFailureListener { e ->
                //sign up failed
                progressDialog.dismiss()
                Toast.makeText(this, "Sign Up failed due to ${e.message}", Toast.LENGTH_SHORT)
                    .show()
            }
    }

    override fun onSupportNavigateUp(): Boolean {
        //go back to previous activity
        onBackPressed()
        return super.onSupportNavigateUp()
    }

}
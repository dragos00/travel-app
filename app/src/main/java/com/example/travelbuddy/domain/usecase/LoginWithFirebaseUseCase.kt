package com.example.travelbuddy.domain.usecase

import com.google.firebase.auth.FirebaseAuth

class LoginWithFirebaseUseCase {

    private var firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    operator fun invoke(
        email: String,
        password: String,
        onSuccess: (String?) -> Unit,
        onError: (String?) -> Unit
    ) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener { onSuccess(it.user?.email) }
            .addOnFailureListener { onError(it.message) }
    }
}

package com.example.travelbuddy.presentation.maps

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.travelbuddy.R
import com.example.travelbuddy.databinding.FragmentMapsBinding
import com.example.travelbuddy.presentation.maps.currency.CurrencyProvider
import com.example.travelbuddy.presentation.maps.location.LocationProvider
import com.example.travelbuddy.presentation.maps.location.SearchProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class MapsFragment : Fragment(R.layout.fragment_maps) {

    private lateinit var binding: FragmentMapsBinding
    private lateinit var locationProvider: LocationProvider
    private lateinit var searchProvider: SearchProvider
    private lateinit var currencyProvider: CurrencyProvider
    private lateinit var googleMap: GoogleMap
    private lateinit var searchedLocation: LatLng

    private val callback = OnMapReadyCallback {
        googleMap = it
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapsBinding.inflate(inflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        setupListeners()

        locationProvider = LocationProvider(requireActivity())
        locationProvider.location.observe(
            viewLifecycleOwner,
            { moveCamera(LatLng(it.latitude, it.longitude)) })

        searchProvider = SearchProvider(requireActivity())
        searchProvider.locationList.observe(viewLifecycleOwner, { address ->
            searchedLocation = LatLng(address[0].latitude, address[0].longitude)

            googleMap.clear()

            searchProvider.highlightBorder(
                address[0].latitude,
                address[0].longitude,
                googleMap,
                requireContext()
            )

            moveCamera(searchedLocation)
        })

        currencyProvider = CurrencyProvider(requireActivity())
        currencyProvider.currencyData.observe(viewLifecycleOwner, {
            Log.d("currency", it)
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        })
    }

    override fun onResume() {
        locationProvider.startLocationUpdates(requireActivity())
        super.onResume()
    }

    override fun onPause() {
        locationProvider.stopLocationUpdates()
        super.onPause()
    }

    private fun moveCamera(coordinates: LatLng) {
        googleMap.addMarker(MarkerOptions().position(coordinates).title("You are here"))
        val cameraPosition = CameraPosition.Builder()
            .target(coordinates)
            .zoom(10f)            // Sets the zoom
            //.bearing(90f)         // Sets the orientation of the camera to east
            .tilt(30f)            // Sets the tilt of the camera to 30 degrees
            .build()              // Creates a CameraPosition from the builder
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun setupListeners() {
        binding.searchEt.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                val location = binding.searchEt.text.toString()
                Toast.makeText(context, "You searched ${location.trim()}", Toast.LENGTH_SHORT)
                    .show()
                searchProvider.searchLocation(location)
                currencyProvider.getCurrency(location)
                binding.searchEt.text.clear()
                return@OnKeyListener true
            }
            false
        })
    }
}
package com.example.travelbuddy.presentation.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.travelbuddy.R
import com.example.travelbuddy.databinding.FragmentLoginBinding
import com.example.travelbuddy.presentation.maps.MapsFragment
import com.example.travelbuddy.presentation.signup.SignUpFragment

class LoginFragment : Fragment(R.layout.fragment_login) {

    companion object {
        const val NAME = "Login"
    }

    private val viewModel by viewModels<LoginViewModel>()

    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        viewModel.viewState.observe(viewLifecycleOwner, { viewState -> handleViewState(viewState) })
        setupListeners()
        super.onStart()
    }

    private fun setupListeners() {
        binding.loginBtn.setOnClickListener { attemptLogin() }
        binding.noAccountTv.setOnClickListener { openSignUpFragment() }
    }

    private fun attemptLogin() {
        val email = binding.emailEt.text.toString()
        val password = binding.passwordEt.text.toString()
        viewModel.attemptLogin(email, password)
    }

    private fun handleViewState(viewState: LoginViewState) {
        when (viewState) {
            is LoginViewState.Error -> {
                Toast.makeText(context, viewState.message, Toast.LENGTH_SHORT).show()
            }
            is LoginViewState.Success -> openMapsFragment(viewState.userEmail)
        }
    }

    private fun openMapsFragment(userEmail: String?) {
        userEmail?.let { Toast.makeText(context, "Logged in as $it", Toast.LENGTH_SHORT).show() }
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(this.id, MapsFragment())
        transaction?.disallowAddToBackStack()
        transaction?.commit()
    }

    private fun openSignUpFragment() {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(this.id, SignUpFragment())
        transaction?.disallowAddToBackStack()
        transaction?.commit()
    }
}

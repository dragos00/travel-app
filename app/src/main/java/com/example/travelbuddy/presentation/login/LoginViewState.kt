package com.example.travelbuddy.presentation.login

sealed class LoginViewState {
    class Success(val userEmail : String?) : LoginViewState()
    class Error(val message: String?) : LoginViewState()
}
package com.example.travelbuddy.presentation.signup

import android.text.TextUtils
import android.util.Patterns
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.travelbuddy.domain.usecase.SignUpWithFirebaseUseCase

class SignUpViewModel : ViewModel() {

    private val signUpWithFirebaseUseCase = SignUpWithFirebaseUseCase()

    val viewState = MutableLiveData<SignUpViewState>()

    fun attemptSignUp(email: String, password: String, reEnterPassword: String) {
        if (isEmailValid(email) && isPasswordValid(password, reEnterPassword)) {
            signUpWithFirebaseUseCase(email, password,
                { userEmail -> viewState.postValue(SignUpViewState.Success(userEmail)) },
                { errorMsg -> viewState.postValue(SignUpViewState.Error(errorMsg)) }
            )
        }
    }

    private fun isEmailValid(email: String): Boolean {
        if (TextUtils.isEmpty(email)) {
            viewState.postValue(SignUpViewState.Error("Please enter email address"))
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            viewState.postValue(SignUpViewState.Error("Invalid email format"))
            return false
        }
        return true
    }

    private fun isPasswordValid(password: String, reEnterPassword: String): Boolean {
        if (TextUtils.isEmpty(password)) {
            viewState.postValue(SignUpViewState.Error("Please enter password"))
            return false
        } else if (TextUtils.isEmpty(reEnterPassword)) {
            viewState.postValue(SignUpViewState.Error("Please Re-Enter password"))
            return false
        } else if (password != reEnterPassword) {
            viewState.postValue(SignUpViewState.Error("Passwords does not match"))
            return false
        }
        return true
    }
}
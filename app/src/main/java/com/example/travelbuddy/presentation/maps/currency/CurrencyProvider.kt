package com.example.travelbuddy.presentation.maps.currency

import android.app.Activity
import android.location.Address
import android.location.Geocoder
import android.util.Log
import androidx.lifecycle.MutableLiveData
import java.util.*

class CurrencyProvider(activity: Activity) {

    var currencyData = MutableLiveData<String>()
    private val geocoder = Geocoder(activity)
    private lateinit var addressList: List<Address>
    private lateinit var address: List<Address>
    private lateinit var countryName: String
    private lateinit var locale: Locale

    fun getCurrency(locationString: String) {
        countryName = getCountryName(locationString)
        val countryCode = getCountryCode(countryName)
        locale = Locale("", countryCode)

        Log.d("countryname", countryName)
        Log.d("currencyLocale", countryCode.toString())

        val currency = Currency.getInstance(locale)

        if (currency != null) {
            currencyData.postValue(currency.currencyCode + " : " + currency.displayName)
        }
    }

    private fun getCountryCode(countryName: String): String? =
        Locale.getISOCountries().find { Locale("", it).displayCountry == countryName }

    private fun getCountryName(locationString: String): String {
        addressList = geocoder.getFromLocationName(locationString.trim(), 1)
        address = geocoder.getFromLocation(addressList[0].latitude, addressList[0].longitude, 1)
        return address[0].countryName
    }
}
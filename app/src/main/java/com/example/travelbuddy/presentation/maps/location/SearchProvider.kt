package com.example.travelbuddy.presentation.maps.location

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.travelbuddy.R
import com.google.android.gms.maps.GoogleMap
import com.google.maps.android.data.geojson.GeoJsonLayer
import com.google.maps.android.data.geojson.GeoJsonPolygonStyle
import org.json.JSONObject
import java.util.*

class SearchProvider(activity: Activity) {

    var locationList = MutableLiveData<List<Address>>()
    private val geocoder = Geocoder(activity, Locale("en", "US"))
    private lateinit var layer2: GeoJsonLayer
    private lateinit var style: GeoJsonPolygonStyle
    private lateinit var countryName: String
    private lateinit var address: MutableList<Address>
    private lateinit var layer: GeoJsonLayer
    private lateinit var jsonObject: JSONObject

    fun searchLocation(locationString: String) {
        if (locationString.trim().isNotEmpty())
            locationList.postValue(geocoder.getFromLocationName(locationString.trim(), 1))
    }

    fun highlightBorder(
        latitude: Double,
        longitude: Double,
        googleMap: GoogleMap,
        context: Context
    ) {
        address = geocoder.getFromLocation(latitude, longitude, 1)
        layer = GeoJsonLayer(
            googleMap, R.raw.borders, context
        )

        if (address.isNotEmpty()) {
            countryName = address[0].countryName
            Log.d("countryName", countryName)

            for (feature in layer.features) {

                if (feature.getProperty("name").equals(countryName)
                ) {
                    jsonObject = JSONObject()

                    layer2 = GeoJsonLayer(googleMap, jsonObject)
                    layer2.addFeature(feature)

                    style = layer2.defaultPolygonStyle
                    style.strokeColor = Color.rgb(255, 118, 118)
                    style.fillColor = Color.argb(40, 0, 213, 255)
                    style.strokeWidth = 7f
                    layer2.addLayerToMap()
                }
            }
        }
    }
}
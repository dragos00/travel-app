package com.example.travelbuddy.presentation

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import com.example.travelbuddy.R
import com.example.travelbuddy.databinding.ActivityMainBinding
import com.example.travelbuddy.presentation.login.LoginFragment
import com.example.travelbuddy.presentation.maps.MapsFragment
import com.example.travelbuddy.presentation.profile.ProfileFragment
import com.example.travelbuddy.presentation.travel.TravelFragment
import com.google.firebase.auth.FirebaseAuth


class MainActivity : AppCompatActivity() {

    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var binding: ActivityMainBinding
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar
    private var firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //this.supportActionBar!!.hide()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.transparent)))
        supportActionBar?.title = null
        setToggleButton()
        handleItems()
        overrideSingOutButton()
    }

    override fun onStart() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.rootLayout, LoginFragment(), LoginFragment.NAME)
            .commit()
        super.onStart()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleItems() {
        binding.navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.profileItem -> {
                    Toast.makeText(
                        applicationContext,
                        "Profile fragment",
                        Toast.LENGTH_SHORT
                    ).show()

                    supportFragmentManager.beginTransaction()
                        .replace(R.id.rootLayout, ProfileFragment())
                        .commit()
                    binding.drawerLayout.closeDrawers()
                }

                R.id.mapsItem -> {
                    Toast.makeText(
                        applicationContext,
                        "Maps",
                        Toast.LENGTH_SHORT
                    ).show()

                    supportFragmentManager.beginTransaction()
                        .replace(R.id.rootLayout, MapsFragment())
                        .commit()
                    binding.drawerLayout.closeDrawers()
                }

                R.id.travelItem -> {
                    Toast.makeText(
                        applicationContext,
                        "Travel details",
                        Toast.LENGTH_SHORT
                    ).show()

                    supportFragmentManager.beginTransaction()
                        .replace(R.id.rootLayout, TravelFragment())
                        .commit()
                    binding.drawerLayout.closeDrawers()
                }
            }
            true
        }
    }

    private fun setToggleButton() {
        toggle = ActionBarDrawerToggle(this, binding.drawerLayout, R.string.open, R.string.close)
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun overrideSingOutButton() {
        binding.signOutButton.setOnClickListener {
            if (firebaseAuth.currentUser != null) {
                firebaseAuth.signOut()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.rootLayout, LoginFragment())
                    .commit()
                binding.drawerLayout.closeDrawers()
            }
        }
    }
}
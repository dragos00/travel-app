package com.example.travelbuddy.presentation.login

import android.text.TextUtils
import android.util.Patterns
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.travelbuddy.domain.usecase.LoginWithFirebaseUseCase

class LoginViewModel : ViewModel() {

    private val loginWithFirebaseUseCase = LoginWithFirebaseUseCase()

    val viewState = MutableLiveData<LoginViewState>()

    fun attemptLogin(email: String, password: String) {
        if (isEmailValid(email) && isPasswordValid(password)) {
            loginWithFirebaseUseCase(email, password,
                //success
                { userEmail -> viewState.postValue(LoginViewState.Success(userEmail)) },
                //error
                { errorMsg -> viewState.postValue(LoginViewState.Error(errorMsg)) }
            )
        }
    }

    private fun isEmailValid(email: String): Boolean {
        if (TextUtils.isEmpty(email)) {
            viewState.postValue(LoginViewState.Error("Please enter email address"))
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            viewState.postValue(LoginViewState.Error("Invalid email format"))
            return false
        }
        return true
    }

    private fun isPasswordValid(password: String): Boolean {
        if (TextUtils.isEmpty(password)) {
            viewState.postValue(LoginViewState.Error("Please enter password"))
            return false
        }
        return true
    }
}
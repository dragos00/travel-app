package com.example.travelbuddy.presentation.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.travelbuddy.R
import com.example.travelbuddy.databinding.FragmentSignUpBinding
import com.example.travelbuddy.presentation.login.LoginFragment
import com.example.travelbuddy.presentation.maps.MapsFragment

class SignUpFragment : Fragment(R.layout.fragment_sign_up) {

    companion object {
        const val NAME = "SignUp"
    }

    private val viewModel by viewModels<SignUpViewModel>()

    private lateinit var binding: FragmentSignUpBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSignUpBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        viewModel.viewState.observe(viewLifecycleOwner, { viewState -> handleViewState(viewState) })
        setupListeners()
        super.onStart()
    }

    private fun setupListeners() {
        binding.signUpBtn.setOnClickListener { attemptSignUp() }
        binding.haveAccountTv.setOnClickListener { openLoginFragment() }
    }

    private fun attemptSignUp() {
        val email = binding.emailEt.text.toString()
        val password = binding.passwordEt.text.toString()
        val reEnterPassword = binding.reEnterPasswordEt.text.toString()
        viewModel.attemptSignUp(email, password, reEnterPassword)
    }

    private fun handleViewState(viewState: SignUpViewState) {
        when (viewState) {
            is SignUpViewState.Error -> {
                Toast.makeText(context, viewState.message, Toast.LENGTH_SHORT).show()
            }
            is SignUpViewState.Success -> {
                openMapsFragment(viewState.userEmail!!)
            }
        }
    }

    private fun openMapsFragment(userEmail: String) {
        Toast.makeText(context, "Logged in as $userEmail", Toast.LENGTH_SHORT).show()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(this.id, MapsFragment())
        transaction?.disallowAddToBackStack()
        transaction?.commit()
    }

    private fun openLoginFragment() {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(this.id, LoginFragment())
        transaction?.disallowAddToBackStack()
        transaction?.commit()
    }
}
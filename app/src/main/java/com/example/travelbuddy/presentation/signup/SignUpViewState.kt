package com.example.travelbuddy.presentation.signup

sealed class SignUpViewState {
    class Success(val userEmail: String?) : SignUpViewState()
    class Error(val message: String?) : SignUpViewState()
}